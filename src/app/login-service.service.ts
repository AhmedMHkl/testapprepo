import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginServiceService {

  private url = environment.url;

  constructor(private http: HttpClient) { }

  public getCode(): Observable<any> {
    return this.http.post(`${this.url}/api/integration/getusercode`, {
      "username": "officeuser",
      "userType": "1"
    })
  }

}
